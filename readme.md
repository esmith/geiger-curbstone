Curbstone Request Builder and Post Response Package
===================


The Curbstone Request Builder and Post Response Package provides an easier and manageable interface to digest the return post data from the client passing Curbstone's Payment Landing Page. A key feature of this package is the consolidation of error codes and messages by way of error handling.

--------------

##### **Before:**
Application developers have to learn (and relearn after a while) many domain-specific codes
```
if( ($MFRTRN=="UG" && $MFRCVV!="M") || ($MFRTRN=="UN" &&  $MFRREF=="82") || ($MFRTRN=="UL" && $MFATAL=="I") ){
	/* is it clear what the conditions are? */
}
```
##### **After:**
User-friendly errors for developers to catch.
```
try{
	$curbstone->validate();
} catch(InvalidCvv $e){
	/* I'm clearly handling what happens when a CVV is invalid. */
}

```


----------


Installation
-------------

Your dotenv configuration file should include all of the required environment variables found in the **sample.env** file.


----------


RequestBuilder Usage
-------------

Pass the cart/order id, user/customer id, and the form data from the payment page: Name on card (MFNAME), street address (MFADD1), and zip code (MFZIPC).  Everything else will be generated or defaulted from the environment variables. This non-user provided data should be considered private information; it should not be sent through responses to the client (eg. through hidden inputs on a form). The entire data collection is packaged and sent to curbstone - which returns a public token to be used by the client to access the Curbstone PLP (Payment Landing Page).
```
$curbstoneRequest = new RequestBuilder( $cartId, $userId, $targetUrl, $formData);
$curbstoneRequest->getSessionToken(); // YW4tZXhhbXBsZQ==
```
The RequestBuilder Package will generate a globally unique Curbstone Order and Customer reference code - provided that the two-character application code is unique and the order and customer ids you are providing are unique to your implementing application (eg. auto-incrementing sql ids).

PostResponse Usage
-------------
[pseudo-code]
```php
public function inbound(Request $request){
    try{
        $data = $request::all();
        $order = Cart::getContext();
        $payment = new CurbstoneCard();
        $payment->prepare($data);
        $purchase = new Purchase($order, $payment);
        $purchase->process();
    }
    // SPEC: MFRTRN = ‘UG’ & MFRCVV <> ‘M’  |  MFRTRN = ‘UN’ & MFRREF = ‘82’
    catch(InvalidCvv $e){
        return $this->breakIframeAndRedirectError('Invalid CVV. Please verify the numbers on the back of your card and try again.');
    }
    catch(CvvNoMatch $e){
        return $this->breakIframeAndRedirectError('Invalid CVV. Please verify the numbers on the back of your card and try again.');
    }
    // SPEC: MFRTRN = ‘UN’ & MFRREF = ‘14’  |  MFGRTN = ‘UL’ & MFATAL = ‘X’ or ‘H’
    catch(InvalidCardNumber $e){
        return $this->breakIframeAndRedirectError('Invalid card number. Please try again.');
    }
    // SPEC: MFRTRN = ‘UN’ & MFRREF = ‘54’  |  MFGRTN = ‘UL’ & MFATAL = ‘J’
    catch(InvalidExpiration $e){
        return $this->breakIframeAndRedirectError('The card has expired. Please try another card.');
    }
    // SPEC: MFGRTN = ‘UL’ & MFATAL = ‘P’
    catch(MissingZipCode $e){
        return $this->breakIframeAndRedirectError('The zip code is missing or did not match. Please try again.');
    }
    // SPEC: All other MFRREF errors
    catch(DeclinedCard $e){
        return $this->breakIframeAndRedirectError('The card was declined. Please contact your card-issuing bank. <span class="hide">'.$e->getMessage().'</span>');
    }
    catch(GenericErrorWithoutDetails $e){
        return $this->breakIframeAndRedirectError('The card was declined. Please contact your card-issuing bank. <span class="hide">'.$e->getMessage().'</span>');
    }
    catch(Exception $e){
        return $this->breakIframeAndRedirectError('There was an error processing your card. '.$e->getMessage());
    }

    // SPEC: MFRTRN = ‘UG’ & MFRCVV = ‘M’
    return $this->breakIframeAndRedirect('checkout.review.index'); //['success'=>'Place your order to process payment']

}
```

> **Note:**

> Curbstone provided a way for their page to communicate from within an iframe to the parent page using window.postMessage().  In general and in good practice, javascript should not be able communicate across websites. Browsers are continually changing and increasing security around this type of behavior (requiring same domain, same protocol, and same port, or simply blocking it altogether. See "Java Applet" for examples of over-stepping system/domain boundaries).  Instead, we will use an iframe with Curbstone's "redirect" method (a typical iframe convention of displaying one website within a frame of another website) where the iframe'd page will redirect back to the application (within the iframe) and then break out of the iframe.

>In order to solve the problem of the iframe showing the target application url within an iframe, we could start to put a conditional top.location.href script at the top of pages (back to the payment page for errors and checkout review), but this starts to get a little messy and has a very undesirable side effect.  While the http request for top.location.href loads, the rest of the page is still loading and rendering the page (within the iframe) for a moment until it redirects. To solve this problem and to keep the solution organized, we dedicate a view page with no body content to render and break out of the iframe to redirect to the intended page. Any messages flashed to this page, will need to be reflashed to the following redirected page.
```
@if(!empty($message))
    {{session()->flash('message', [key($message) => reset($message)])}}
@endif
<html>
    <head>
        <script>
            // break out of curbstone iframe 
            // redirect to payment or review page
            (function(){
                if (top.location != location){
                    var uri = '{{$target}}';
                    top.location.href = uri;
                }
            })();
        </script>
    </head>
</html>
```