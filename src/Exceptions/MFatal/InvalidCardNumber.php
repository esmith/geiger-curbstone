<?php

namespace Curbstone\Exceptions\MFatal;

use Curbstone\Exceptions\AbstractHandler;

class InvalidCardNumber extends AbstractHandler{

	public function __construct($msg=''){
		parent::__construct('The card number was invalid. '. $msg);
	}

}