<?php

namespace Curbstone\Exceptions\MFatal;

use Curbstone\Exceptions\AbstractHandler;

class InvalidCvv extends AbstractHandler{

	public function __construct($msg=''){
		parent::__construct('The CVV was invalid. '. $msg);
	}

}