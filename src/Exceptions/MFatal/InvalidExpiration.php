<?php

namespace Curbstone\Exceptions\MFatal;

use Curbstone\Exceptions\AbstractHandler;

class InvalidExpiration extends AbstractHandler{

	public function __construct($msg=''){
		parent::__construct('The expiration date was invalid. '. $msg);
	}

}