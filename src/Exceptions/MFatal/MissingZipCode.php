<?php

namespace Curbstone\Exceptions\MFatal;

use Curbstone\Exceptions\AbstractHandler;

class MissingZipCode extends AbstractHandler{
    
    public function __construct($msg=''){

        parent::__construct("Missing zip code. ".$msg);
    }

}