<?php

namespace Curbstone\Exceptions;

use Curbstone\Exceptions\AbstractHandler;
use Curbstone\Exceptions\GenericErrorWithoutDetails;
use Curbstone\Exceptions\MFatal\InvalidCvv;
use Curbstone\Exceptions\MFatal\InvalidExpiration;
use Curbstone\Exceptions\MFatal\MissingZipCode;
use Curbstone\Exceptions\MFatal\InvalidCardNumber;

class MFatal extends AbstractHandler{

    public function __construct($code){

        switch($code){
            case "I":
                throw new InvalidCvv();
            break;
            case "J":
                throw new InvalidExpiration();
            break;
            case "P":
                throw new MissingZipCode();
            break;
            case "X":
            case "H":
                throw new InvalidCardNumber();
            break;
            default:
                $messages = static::loadMessageCodes('mfatal');
                $msg = static::getMessageByCode($code, $messages);
                if(empty($msg)){
                    throw new GenericErrorWithoutDetails(" Undefined MFatal Exception. '".$code."' not defined in mfatal.json");
                }
                parent::__construct('An error occurred while processing the credit card: '. $msg);
        }

    }

}