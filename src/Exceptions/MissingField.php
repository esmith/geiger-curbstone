<?php

namespace Curbstone\Exceptions;

use Curbstone\Exceptions\AbstractHandler;

class MissingField extends AbstractHandler{
	
	public function __construct($msg=''){
		parent::__construct("A required field is missing from curbstone's forward post request through the client: ".$msg);
	}

}