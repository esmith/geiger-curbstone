<?php

namespace Curbstone\Exceptions;

use Curbstone\Exceptions\AbstractHandler;
use Curbstone\Exceptions\MFatal\InvalidCardNumber;
use Curbstone\Exceptions\MFatal\InvalidCvv;
use Curbstone\Exceptions\MFatal\InvalidExpiration;

class DeclinedCard extends AbstractHandler{
    
    public function __construct($code){

    	switch($code){
    		// SPEC: MFRTRN="UN" & MFRREF="14" : "Invalid card number. Resubmit payment information"
    		case "14":
    			throw new InvalidCardNumber('MFRTRN=UN and MFRREF=14');
    		break;
    		// SPEC: MFRTRN="UN" & MFRREF="82" : "Invalid CVV. Resubmit payment information"
			case "82":
				throw new InvalidCvv('MFRTRN=UN and MFRREF=82');
			break;
			// SPEC: MFRTRN="UN" & MFRREF="54" : "Card Expired. Resubmit payment information"
			case "54":
				throw new InvalidExpiration('MFRTRN=UN and MFRREF=54');
			break;
			// SPEC: MFRTRN="UN" & MFRREF="54" :"Card was declined. Please contact your card-issuing bank."
			default:
				parent::__construct('The card was declined (MFRTRN=UN), but no information was provided (MFRREF). ' . $code);
    	}

    }

}