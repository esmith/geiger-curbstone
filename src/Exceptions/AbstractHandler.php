<?php

namespace Curbstone\Exceptions;

use Exception;
use Curbstone\Traits\ErrorJsonLoaderAndMapper;

abstract class AbstractHandler extends Exception{

    use ErrorJsonLoaderAndMapper;

}