<?php

namespace Curbstone\Exceptions;

use Curbstone\Exceptions\AbstractHandler;

class GenericErrorWithoutDetails extends AbstractHandler{
	
	public function __construct($msg=''){
		parent::__construct('The payment transaction failed, but no reason was provided.'. $msg);
	}

}