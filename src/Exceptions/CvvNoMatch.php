<?php

namespace Curbstone\Exceptions;

use Curbstone\Exceptions\AbstractHandler;
use Curbstone\Exceptions\MFatal\InvalidCvv;

class CvvNoMatch extends AbstractHandler{
    
    public function __construct($code){

        $messages = static::loadMessageCodes('cvv');
        $msg = static::getMessageByCode($code, $messages);

        throw new InvalidCvv($msg);
        // parent::__construct('CVV Error: '.$msg);
        
    }

}