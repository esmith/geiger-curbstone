<?php

namespace Curbstone;

use Curbstone\Exceptions\CvvNoMatch;
use Curbstone\Exceptions\DeclinedCard;
use Curbstone\Exceptions\GenericErrorWithoutDetails;
use Curbstone\Exceptions\MFatal;
use Curbstone\Exceptions\MissingField;

use Curbstone\Traits\ErrorJsonLoaderAndMapper;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class PostResponse{

    use ErrorJsonLoaderAndMapper;

    private $dataFields;
    private $dataFieldDefinitions;
    private $requireCvvMatch = true;
    private $log;

    public function __construct($dataFields=[]){
        $this->dataFields = $dataFields;
        $this->fill($this->dataFields);
        $this->log = new Logger('Curbstone');
        $this->log->pushHandler(new StreamHandler(storage_path().'/logs/curbstone-client-post.log'));
        $this->log->info('POST', [$dataFields]);
    }


    // == Unsupported as it requires Curbstone to prevent emitted cvv errors and triggering MFRTRN->UN->MFRREF, MFRTRN->UL->MFATAL
    public function setRequireCvvMatch($bool=true){
        $this->requireCvvMatch = $bool;
    }

    public function validate(){

        // Only require fields when the transaction succeeds. 
        // Many fields are missing when Curbstone produces an error
        if($this->MFRTRN === 'UG'){
            // Ensure we're getting back key data (such as a transaction #)
            $this->_hasRequiredFieldsOrFail();
            $this->log->debug('Curbstone response has all the required fields');
        }

        // Check transaction status (MFRTRN) and throw appropriate errors
        if(!$this->_isSuccessful()){
            $this->_throwUnsuccessfulException();
        }
        $this->log->debug('Statuses indicate a successful transaction');

        // SPEC: When MFRTRN = "UG" & MFRCVV <> "M" : "Require user to resubmit payment"
        // Even when the transaction is successful by Curbstone, we're not going to accept an
        // invalid CVV (except for AMEX because they currently do not check CVVs for that vendor)
        if($this->MFRVNA !== 'AMEX'){
            $msg = 'Card is not AmEx. Require CVV check';
            $this->log->debug($msg);
            if( $this->requireCvvMatch && !$this->_cvvIsMatch()){
                throw new CvvNoMatch($this->MFRCVV);
            }
            $this->log->debug('CVV match passes validation');
        }else{
            $this->log->debug('CVV check skipped for Amex card');
        }

        return true;

    }

    /*
     * Get the application's order id (Eg. "PU-1038" => 1038)
     */
    public function getOrderId(){
        return substr($this->MFORDR, strpos($this->MFORDR, env('CURBSTONE_2CHAR_APP_CODE')."-") - 1);
    }

    /*
     * Get the application's customer id (Eg. "PUc38192" => 38192)
     */
    public function getUserId(){
        return substr($this->MPCUST, strpos($this->MPCUST, env('CURBSTONE_2CHAR_APP_CODE')."c") - 1);
    }

    public function getChargedAmount(){
        return $this->MFAMT1;
    }

    public function getMaskedNumber(){
        return $this->MFCARD;
    }

    public function getCardVendor(){
        return $this->MFRVNA;
    }

    public function getExpiration(){
        return $this->MFEDAT;
    }

    /**
     * Return an object that provides information about a coded field (eg. MFRTRN, MFORDR, MFRREF)
     * @param  string $code
     * @return object Field Definition as defined in fields.json
     */
    public function getResponseFieldDefinitionByCode($code){
        foreach($this->_getResponseFieldDefinitions() as $field){
            if($code === $field->code){
                return $field;
            }
        }
        return false;
    }

    protected function fill($data){
        $codes = array_pluck($this->_getResponseFieldDefinitions(), 'code');
        foreach($data as $k=>$v){
            if(in_array($k, $codes)){
                $this->$k = $v;
            }
        }
    }

    /**
     * Get a list of expected POST field parameters with field description and length information (if available)
     * @return array Field objects containing the code and name (length, required, type...)
     */
    private function _getResponseFieldDefinitions(){
        if(empty($this->dataFieldDefinitions)){
            $this->dataFieldDefinitions = $this->loadMessageCodes('fields'); // ResponseCodes/fields.json
        }
        return $this->dataFieldDefinitions;
    }

    /*
     * Ensure the code provided has a value
     */
    private function _throwIfEmptyCode($code){
        $field = $this->getResponseFieldDefinitionByCode($code);
        // Accept falsey values (eg. AMT=0)
        if( !isset($this->$code) || strlen($this->$code) <= 0 ){
            //except for Amex CVV error
            if($code !== 'MFRCVV' && $this->MFRVNA !== 'AMEX'){
                $this->log->debug('Empty field error', [$code, $this->$code]);
                if($code === 'MFRCVV'){
                    throw new CvvNoMatch($this->MFRCVV);
                }
                throw new MissingField($field->code .' ('.$field->name.')');
            }
        }
    }

    private function _isSuccessful(){
        // SPEC: When MFRTRN = "UG" & MFRCVV = "M" : "Proceed to Order Screen" 
        return $this->MFRTRN === 'UG'; // Transaction Code "UG" (U Good)
    }

    private function _throwUnsuccessfulException(){

        /*
         * Transaction Code "UN"  (U Nogood)
         * Errors provided in MFRREF Code
         */
        if($this->MFRTRN === 'UN'){
            $mfrref = isset($this->MFRREF) ? $this->MFRREF : '';
            $msg = 'MFRTRN=UN, looking to MFRREF for more details on error.';
            $this->log->debug($msg, [$mfrref]);
            // SPEC: MFRTRN = "UN" (See DeclinedCard for exceptions handling MFRREF)
            throw new DeclinedCard($mfrref);
        }

        /*
         * Transaction Code "UL"  (U Lose)
         * Errors provided in MFATAL Code
         */
        if(!empty($this->MFATAL) && $this->MFRTRN === 'UL'){
            $msg = 'MFRTRN=UL, looking to MFATAL for more details on error.';
            $this->log->debug($msg);
            throw new MFatal($this->MFATAL);
        }

        /*
         * A 'catch all' generic error if/when none is defined by the api
         */
        $msg = "MFRTRN provided an unrecognized response code. Anything other than 'UG' is interpretted as an error";
        $this->log->debug($msg);
        throw new GenericErrorWithoutDetails();
    }

    /*
     * Determine CVV Match by MFRCVV="M"
     */
    private function _cvvIsMatch(){
        return !empty($this->MFRCVV) && $this->MFRCVV === 'M';
    }

    private function _hasRequiredFieldsOrFail(){
        // Response Fields required for processing the transaction 
        $requiredResponseFields = [
            'MFAPPR', // Auth approval code (is blank when no there's no cvv for AmEx)
            'MFRTRN', // Response Code (Success or error or error)
            'MFORDR', // POPUP Order Reference number
            'MFAMT1', // Transaction amount (0.00)
            'MFNAME', // Name on card
            'MFADD1', // Address
            'MFZIPC', // Zip code
            'MFRAVS', // AVS response code (is empty when these is no error)
            'MFUKEY', // Token
            'MFCARD', // Masked card number
            'MFRCVV', // CVV response code (success or error...)
            'MFRVNA', // Card vendor
            'MFEDAT', // Expiration Date (mmyy)
        ];
        foreach($requiredResponseFields as $field){
            $this->_throwIfEmptyCode($field);
        }
        return true;
    }

}