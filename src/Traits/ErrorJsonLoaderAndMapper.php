<?php

namespace Curbstone\Traits;

trait ErrorJsonLoaderAndMapper{

	protected static function loadMessageCodes($filename){
		return json_decode(file_get_contents(__DIR__.'/../ResponseCodes/'.$filename.'.json'));
	}

	public static function getMessageByCode($code, $array){
		foreach($array as $error){
			if($code === $error->code){
				return $error->message;
			}
		}
		return '';
	}

}