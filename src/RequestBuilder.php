<?php

namespace Curbstone;

use Route;

/*
 * 
 */
class RequestBuilder{

    // Unique Application Code (2-characters)
    public $APPCODE;

    // Curbstone's PLP url
    public $MFTRGT;

    // Curbstone's Customer # for Geiger
    public $MFCUST;

    // Curbstone's Merchant Code
    public $MFMRCH;

    // Transaction Type
    public $MFTYPE;

    // Secondary Transaction Type
    public $MFTYP2;

    // Entry Method
    public $MFMETH;

    // Transaction Amount
    public $MFAMT1 = 0.00;

    // Return Target URL
    public $MPTRGT;

    // Invoice Number (api seems to generate a response for us)
    public $MFREFR = '';

    // Generated Order Number (App+Order+Customer)
    public $MFORDR;

    // Application Customer Number
    public $MPCUST;

    // Session Token
    public $MSESS;

    // Iframe (js postMessage on target) or Redirect
    public $mode = 'redirect';

    /*
     * Data that can be passed through the constructor
     */
    private $fillable = ['MFNAME','MFADD1','MFZIPC','mode'];

    /*
     * Data sent to Curbstone
     */
    private $sendable = ['MFCUST', 'MFMRCH', 'MFTYPE', 'MFTYP2', 'MFMETH', 'MFAMT1', 'MPTRGT', 'MFREFR', 'MFORDR', 'MPCUST', 'MFNAME', 'MFADD1', 'MFZIPC', 'mode'];

    /*
     * Request Data provides the option to pass variable user data:
     * ['MFNAME'=>'Evan Smith', 'MFADD1'=>'70 Mt Hope Ave', 'MFZIPC'=>'04240']
     */
    public function __construct($orderId, $customerId, $targetUrl, $requestData=[]){

	if(!function_exists('curl_version')){
		throw new \Exception('Curbstone RequestBuilder requires curl to be installed.');
	}

        $this->APPCODE = env('CURBSTONE_2CHAR_APP_CODE');
        $this->MFCUST = env('CURBSTONE_MFCUST');
        $this->MFMRCH = env('CURBSTONE_MFMRCH');
        $this->MFTYPE = env('CURBSTONE_MFTYPE');
        $this->MFTYP2 = env('CURBSTONE_MFTYP2');
        $this->MFMETH = env('CURBSTONE_MFMETH');
        $this->MFTRGT = env('CURBSTONE_MFTRGT');
        $this->MPTRGT = $targetUrl;
        
        $this->setCustomerReferenceCode($customerId);
        $this->setOrderReferenceCode($orderId, $customerId);

        $this->fill($requestData);

    }

    public function getSessionToken(){
        if(empty($this->MSESS)){
            $data = $this->_encodeSendableData();
            $params = http_build_query($data);
            $this->MSESS = $this->_PostToCurbstoneForSessionToken($params);
        }
        return $this->MSESS;
    }

    public function getOrderReferenceCode(){
        return $this->MFORDR;
    }

    public function getCustomerReferenceCode(){
        return $this->MPCUST;
    }

    public function getPlpUrl(){
        return $this->MFTRGT;
    }

    public function getTargetUrl(){
        return $this->MPTRGT;
    }

    // Generate a globally unique order id (16 character max)
    // Modify Warning: This convention is parseable in PostResponse::getOrderId()
    public function setOrderReferenceCode($orderId, $customerId){
        return $this->MFORDR = $this->APPCODE.'-'.$orderId;
    }

    // Generate a globally unique customer id (10 character max)
    // Modify Warning: This convention is parseable in PostResponse::getCustomerId()
    public function setCustomerReferenceCode($customerId){
        return $this->MPCUST = $this->APPCODE.'c'.$customerId;
    }

    public function fill($requestData){
        foreach($requestData as $k=>$v){
            if(in_array($k, $this->fillable) && strlen($v) > 0){
                $this->$k = $v;
            }
        }
    }

    private function _encodeSendableData(){
        $params=[];
        if(empty($this->MSESS)){
            foreach(get_object_vars($this) as $key=>$value){
                if(in_array($key, $this->sendable)){
                    $params[$key] = $value; //urlencode($value);
                }
            }
        }
        return $params;
    }

    private function _PostToCurbstoneForSessionToken($paramString){
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => env('CURBSTONE_MFTRGT').'?action=init',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $paramString,
        ]);
        $content = curl_exec($ch);
        try{
            $data = json_decode($content);
            if(isset($data->MFSESS)){
                return $data->MFSESS;
            }

        } catch(Exception $e){
            dd($content);
        }
    }

}
